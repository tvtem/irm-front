import React from 'react';
import ObjectArea from '../components/object/object-area';
import ObjectCreator from '../components/object/object-create';

export default class Core extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      areas: props.areas ? props.areas : false,
      selected: props.selected ? props.selected : false
    };
  }

  componentWillReceiveProps(props) {
    this.setState({
      areas: props.areas ? props.areas : false,
      selected: props.selected ? props.selected : false
    });
  }

  getRooms() {
    const { areas, selected } = this.state;

    if (areas) {
      return areas.map((area, i) => {
        return <ObjectArea key={i} data={area} selected={selected} />;
      });
    }
  }

  render() {
    return (
      <div id='isometric-space' className='static'>
        {this.getRooms()}
        <ObjectCreator />
      </div>
    );
  }
}
