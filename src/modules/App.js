import React, { Component } from 'react';
import '../sass/style.scss'
import Login from '../components/pages/login'
import Projects from '../components/pages/projects/'
import * as Settings from '../settings'

class App extends Component {
  render() {
    Settings.getUserData()

    return (
      <Login>
        <Projects />
      </Login>
    );
  }
}

export default App;
