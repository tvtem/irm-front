import React from 'react';

export default class Globals {
  static Timers = {};
  static ElementConnector = {};

  static Settings = {
    currentScale: 50,
    GridSize: 50,
    UnmoveClasses: [
      'element-controls',
      'element-insert',
      'fas fa-plus-square',
      'btn btn-primary',
      'fas fa-check',
      'fas fa-times',
      'element-insert-details',
      'fas fa-arrow-alt-right right',
      'fas fa-arrow-alt-up up',
      'fas fa-arrow-alt-down down',
      'fas fa-arrow-alt-left left',
      'element',
      'element-insert-tile'
    ]
  };

  static toggle3DWorld() {
    document.getElementById('isometric-space').classList.toggle('static');
  }

  static backToWorld() {
    Globals.ElementConnector._currentWorkArea.setObjectInsertInfo(false);
    Globals.ElementConnector._currentWorkArea = false;
    Globals.ElementConnector._controller.hideController();
    document.getElementById('isometric-space').classList.add('static');
  }

  static clearZoom() {
    Globals.ElementConnector._controller.clearZoom();
  }

  static loader() {
    return (
      <div className='loader'>
        <div className='indicator'>
          <svg width='16px' height='12px'>
            <polyline id='back' points='1 6 4 6 6 11 10 1 12 6 15 6'></polyline>
            <polyline
              id='front'
              points='1 6 4 6 6 11 10 1 12 6 15 6'
            ></polyline>
          </svg>
        </div>
      </div>
    );
  }
}
