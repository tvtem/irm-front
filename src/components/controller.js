import React from 'react';
import Globals from '../modules/Globals';

export default class Controllers extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showController: false,
      objectManager: false,
      objectCreate: false,
      moving: false,
      moved: false,
      modPos: {},
      mousePos: {},
      mouseDown: false,
      lastUpdateViewEvent: false
    };

    let _controller = this;
    Globals.ElementConnector._controller = _controller;

    window.onmousedown = function() {
      _controller.setState({ mouseDown: true });
    };
    window.onmouseup = function() {
      _controller.setState({ mouseDown: false });
    };

    document
      .getElementById('workspace')
      .addEventListener('mousedown', event => {
        _controller.moveStart(event);
      });

    document.getElementById('workspace').addEventListener('mouseup', event => {
      _controller.moveEnd(event);
    });

    document
      .getElementById('workspace')
      .addEventListener('mouseleave', event => {
        _controller.moveEnd(event);
      });

    document
      .getElementById('workspace')
      .addEventListener('mousemove', event => {
        _controller.updateModView(event);
      });

    window.addEventListener('mousewheel', event => {
      _controller.scrollZoom(event);
    });

    Globals.clearZoom();
  }

  showController() {
    this.setState({
      showController: true
    });
  }

  hideController() {
    this.setState({
      showController: false
    });
  }

  scrollZoom(e) {
    let down = e.wheelDelta < 0;
    if (down) {
      this.zoomOut();
    } else this.zoomIn();
  }

  moveStart(event) {
    this.setState({
      moved: false
    });
    let eclass = event.target.className;
    if (Globals.Settings.UnmoveClasses.includes(eclass)) return false;

    //console.log(eclass)

    let rect = document.getElementById('mod').getBoundingClientRect();
    let rpy = event.clientY - rect.top;
    let rpx = event.clientX - rect.left;
    this.setState({ moving: true, modPos: { top: rpy, left: rpx } });
  }

  moveEnd(event) {
    this.setState({ moving: false });
  }

  sanitizeView(x, y) {
    let scale = Globals.Settings.currentScale;
    let limit = scale / 100;
    // limit X
    if (x > 600 * limit) x = 600 * limit;
    if (x < -600 * limit) x = -600 * limit;

    // limit Y
    if (y > 400 * limit) y = 400 * limit;
    if (y < -400 * limit) y = -400 * limit;

    return { x, y };
  }

  updateModView(event, forceUpdate = false) {
    let lastUpdateViewEvent = event;
    if ((this.state.moving && this.state.mouseDown) || forceUpdate) {
      let x = this.state.modPos.left;
      let y = this.state.modPos.top;
      let mod = document.getElementById('mod');

      let san = this.sanitizeView(event.clientX - x, event.clientY - y);

      mod.style.left = `${san.x}px`;
      mod.style.top = `${san.y}px`;
      this.setState({
        moved: true,
        lastUpdateViewEvent
      });
    }
  }

  scale = 100;

  clearScale() {
    let classes = document.getElementById('mod').classList;
    for (let i = 0; i < classes.length; i++) {
      let c = classes[i];
      if (c.startsWith('scale'))
        document.getElementById('mod').classList.remove(c);
    }
  }

  zoomIn() {
    if (Globals.Settings.currentScale >= 300) return false;
    this.clearScale();
    Globals.Settings.currentScale += 10;
    document
      .getElementById('mod')
      .classList.add('scale' + Globals.Settings.currentScale);
    if (this.state.lastUpdateViewEvent)
      this.updateModView(this.state.lastUpdateViewEvent, true);
  }

  zoomOut() {
    if (Globals.Settings.currentScale <= 10) return false;
    this.clearScale();
    Globals.Settings.currentScale -= 10;
    document
      .getElementById('mod')
      .classList.add('scale' + Globals.Settings.currentScale);
    if (this.state.lastUpdateViewEvent)
      this.updateModView(this.state.lastUpdateViewEvent, true);
  }

  clearZoom() {
    this.clearScale();
    document
      .getElementById('mod')
      .classList.add('scale' + Globals.Settings.currentScale);
  }

  toggleGrid() {
    document.getElementById('mod').classList.toggle('dev');
  }

  toggleObjectCreator() {
    let creator = Globals.ElementConnector._objectCreator;
    creator.toggle();
  }

  getControlBar() {
    if (!this.state.showController) return null;

    return (
      <div className='controlBar'>
        <div className='control' onClick={this.zoomIn.bind(this)}>
          <i className='far fa-search-plus' />
        </div>

        <div className='control' onClick={this.zoomOut.bind(this)}>
          <i className='far fa-search-minus' />
        </div>

        <div className='control' onClick={this.toggleObjectCreator.bind(this)}>
          <i className='fas fa-plus-square' />
        </div>

        <div className='control' onClick={this.toggleGrid.bind(this)}>
          <i className='far fa-th' />
        </div>

        <div className='control' onClick={Globals.toggle3DWorld.bind(this)}>
          <i className='far fa-magic' />
        </div>

        <div className='control separator' />

        <div
          className='control aside-right'
          onClick={Globals.backToWorld.bind(this)}
        >
          <i className='fas fa-times' />
        </div>
      </div>
    );
  }

  render() {
    return this.getControlBar();
  }
}
