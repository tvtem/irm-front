import React from 'react'
import Globals from '../modules/Globals'

export default class Grid extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            children: props.children ? props.children : null,
            class: props.className ? props.className : '',
            resized: true,
            width: props.width ? props.width : 1,
            height: props.height ? props.height : 1,
        }

        let _this = this
        window.addEventListener("resize", () => {
            _this.resize()
        });
    }
    componentWillReceiveProps(props) {
        this.setState({
            children: props.children ? props.children : null,
            class: props.className ? props.className : '',
            width: props.width ? props.width : 1,
            height: props.height ? props.height : 1,
        })
    }

    resize() {
        this.setState({
            resized: true
        })
    }

    getGridBg() {
        let scale = Globals.Settings.GridSize
        let bg = []

        let ww = this.state.width
        let wh = this.state.height

        let total = ww * wh

        for (let i = 0; i < total; i++) {
            bg.push(<span key={i} className="grid-mark" style={{ width: scale, height: scale }}></span>)
        }

        return (
            <div id="grid-bg" className="grid-background" style={{ margin: 0 }}>{bg}</div>
        )
    }

    getWorkArea() {
        let width = this.state.width * Globals.Settings.GridSize
        let height = this.state.height * Globals.Settings.GridSize

        return (
            <div className={`grid-wrapper ${this.state.class}`} style={{ width: width, height: height }}>
                {this.getGridBg()}
                {this.state.children}
            </div >
        )
    }

    render() {
        return this.getWorkArea()
    }
}