import Globals from '../../modules/Globals';

export function calcAmbientArea(infoAmbient) {
  const width = infoAmbient.width * Globals.Settings.GridSize;
  const height = infoAmbient.height * Globals.Settings.GridSize;
  const X = infoAmbient.x * Globals.Settings.GridSize + 0;
  const Y = infoAmbient.y * Globals.Settings.GridSize + 0;
  return { width, height, X, Y };
}

export function calcObjectArea(object) {
  const size = object.size.split('x');
  const width =
    Number.parseInt(size[object.flip ? 1 : 0], 10) * Globals.Settings.GridSize;
  const height =
    Number.parseInt(size[object.flip ? 0 : 1], 10) * Globals.Settings.GridSize;
  const x = object.X * Globals.Settings.GridSize;
  const y = object.Y * Globals.Settings.GridSize;
  return { width, height, x, y };
}
