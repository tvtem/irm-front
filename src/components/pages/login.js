import React from 'react';
import * as Services from '../../services';
import { FormGroup, FormControl } from 'react-bootstrap';
import Noty from 'noty';

export default class LoginPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      locked: true
    };
  }

  async doLogin() {
    let user = document.getElementById('username').value;
    let pass = document.getElementById('password').value;

    if (!user || !pass || user.length < 4 || pass.length < 4) {
      new Noty({
        title: 'aaa',
        text: 'bbb',
        layout: 'ccc',
        timeout: 'ddd',
        type: 'eee'
      }).show();
      return false;
    }

    let data = {
      email: user,
      password: pass
    };

    let login = await Services.Auth.doLogin(data);

    if (login) {
      let locked = false;
      this.setState({ locked });
    }
  }

  componentWillMount() {
    this.verifyAccess();
  }

  async verifyAccess() {
    let unlocked = await Services.Auth.unlockPage();
    if (unlocked) {
      this.setState({
        locked: false
      });
    } else {
    }
  }

  getAccessForm() {
    return (
      <div className='loginWrapper'>
        <div className='col-md-12'>
          <div className='modal-dialog modal-sm'>
            <div className='modal-content'>
              <div className='panel-heading'>
                <h3 className='panel-title'>Login</h3>
              </div>
              <div className='panel-body'>
                <form
                  onSubmit={e => {
                    e.preventDefault();
                  }}
                >
                  <fieldset>
                    <FormGroup>
                      <FormControl
                        type='text'
                        defaultValue=''
                        placeholder='Email'
                        onChange={() => {}}
                        id='username'
                      />
                    </FormGroup>
                    <FormGroup>
                      <FormControl
                        type='password'
                        defaultValue=''
                        placeholder='Password'
                        onChange={() => {}}
                        id='password'
                      />
                    </FormGroup>
                    <button
                      className='btn btn-block btn-success'
                      onClick={this.doLogin.bind(this)}
                    >
                      Login
                    </button>
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  getContent() {
    const { children } = this.props;
    const { locked } = this.state;

    return locked ? this.getAccessForm() : children;
  }

  render() {
    return this.getContent();
  }
}
