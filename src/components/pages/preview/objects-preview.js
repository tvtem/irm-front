import React from 'react';
import { calcObjectArea } from '../../utils/calculos';

const ObjectPreview = ({ object, onClick }) => {
  const { width, height, x, y } = calcObjectArea(object);

  return (
    <>
      <div
        id={object.id_object}
        className={`element ${object.flip ? 'flip' : ''}`}
        style={{
          width: width,
          height: height,
          left: x,
          top: y,
          zIndex: object.zIndex
        }}
        onClick={object.zIndex === 100 ? onClick : ''}
      >
        <div
          className={`element-tile ${
            object.zIndex === 100 ? 'element-focus' : ''
          }`}
          style={{
            backgroundImage: `url(${process.env.PUBLIC_URL}/${object.img})`,
            width: object.imgWidth,
            height: object.imgHeight,
            left: object.flip ? object.imgFlipX : object.imgX,
            top: object.flip ? object.imgFlipY : object.imgY
          }}
        ></div>
      </div>
    </>
  );
};

export default ObjectPreview;
