import React, { useEffect, useState } from 'react';
import { Api } from '../../../services/index';
import ObjectPreview from './objects-preview';
import { calcAmbientArea } from '../../utils/calculos';
import { Helmet } from 'react-helmet';
import { Modal } from 'react-bootstrap';

const Preview = props => {
  const id_ambient = props.match.params.id;

  const [show, setShow] = useState(false);
  const [infoAmbient, setAmbientInfos] = useState([]);
  const [titulo, setTitulo] = useState('Não carregou o titulo');
  const [mensagem, setMensagem] = useState('Não carregou a mensagem');
  const [objectsAmbient, setObjects] = useState([]);

  useEffect(() => {
    findAmbientInfo();
    findAmbientObjects();
  }, []);

  if (!infoAmbient) {
    window.location.pathname = '/404';
  }

  const findAmbientObjects = async () => {
    const objects_ambient = await Api.post('ambients/object/list', {
      id_ambient: id_ambient
    });
    const objects = objects_ambient.status === 200 ? objects_ambient.data : [];
    setObjects(objects);
  };

  const findAmbientInfo = async () => {
    const info_ambient = await Api.get('ambients', {
      project: id_ambient
    });

    const infos = info_ambient.data[0];
    setAmbientInfos(infos);
  };

  const onClickObject = async event => {
    const idObject = event.currentTarget.id;

    const objectMessages = await Api.post('message/find', {
      idObject: idObject
    });

    const { titulo, mensagem } = objectMessages.data[0];
    setShow(true);
    setTitulo(titulo);
    setMensagem(mensagem);
  };

  const ModalMessage = () => {
    return (
      <>
        <Modal show={show} onHide={() => setShow(false)}>
          <Modal.Header>
            <p>{titulo}</p>
          </Modal.Header>
          <Modal.Body>
            <p>{mensagem}</p>
          </Modal.Body>
        </Modal>
      </>
    );
  };

  const { width, height, X, Y } = calcAmbientArea(infoAmbient);

  return (
    <>
      <Helmet>
        <title>{infoAmbient.name}</title>
      </Helmet>

      <div className='scale60'>
        <div id='appRoot' style={{ background: 'none' }}>
          <div id='isometric-space'>
            <div
              className='workArea active'
              style={{
                left: X,
                top: Y,
                width: width,
                height: height
              }}
            >
              {objectsAmbient[0] &&
                objectsAmbient.map(object => (
                  <ObjectPreview
                    object={object}
                    onClick={onClickObject}
                  ></ObjectPreview>
                ))}
              <ModalMessage></ModalMessage>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Preview;
