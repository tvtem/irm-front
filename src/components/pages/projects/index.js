import React from 'react';
import * as Services from '../../../services';
import Controller from '../../controller';
import ObjectAreaManager from '../../object/object-area-manager';
import { ListGroup, ListGroupItem } from 'react-bootstrap';
import './style.scss';

export default class ProjectPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      project: 0,
      data: false
    };
  }

  componentWillMount() {
    this.loadProjects();
  }

  selectProject = project => this.setState({ project });

  async loadProjects() {
    let req = await Services.Api.get('projects');

    if (req) {
      this.setState({ data: req.data });
    }
  }

  getProjectList() {
    const { data } = this.state;
    return Object.values(data).map((p, i) => {
      return (
        <ListGroupItem
          bsStyle='info'
          key={i}
          header={p.name}
          onClick={() => this.selectProject(p.id)}
        >
          {p.description}
        </ListGroupItem>
      );
    });
  }

  getProjects() {
    const { project } = this.state;

    if (project)
      return (
        <>
          <Controller />
          <ObjectAreaManager id={project} />
        </>
      );

    return (
      <div className='loginWrapper'>
        <div className='col-md-12'>
          <div className='modal-dialog'>
            <div className='modal-content'>
              <div className='panel-heading'>
                <h3 className='panel-title'>Projetos</h3>
              </div>
              <div className='panel-body'>
                <ListGroup bsClass='list-group project-list'>
                  {this.getProjectList()}
                </ListGroup>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  render() {
    return this.getProjects();
  }
}
