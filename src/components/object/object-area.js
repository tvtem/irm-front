import React from 'react';
import WorkAreaObjectInsert from './object-insert';
import WorkAreaObjects from './objects';
import Globals from '../../modules/Globals';
import Grid from '../grid';
import { Api } from '../../services';
import { calcAmbientArea } from '../utils/calculos';

export default class WorkArea extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      objects: [],
      data: props.data ? props.data : '',
      selected: props.selected ? props.selected : false,
      insertObjectInfo: false
    };
  }

  componentWillReceiveProps(props) {
    this.setState({
      data: props.data ? props.data : '',
      selected: props.selected ? props.selected : false
    });
  }

  async componentDidMount(props) {
    const objects = await this.findObjects();
    this.setState({ objects: objects });
  }

  findObjects = async () => {
    const id_ambient = this.state.data.id;

    const { data: objects } = await Api.post('ambients/object/list', {
      id_ambient
    });

    return await objects;
  };

  setActiveWorkArea() {
    if (
      !Globals.ElementConnector._currentWorkArea &&
      !Globals.ElementConnector._controller.state.moved
    ) {
      Globals.ElementConnector._currentWorkArea = this;
      Globals.toggle3DWorld();
      Globals.ElementConnector._controller.showController();
      this.setState({
        updated: true
      });
    }
  }

  addObject(object) {
    let objs = this.state.objects;
    objs.push(object);

    this.setState({
      objects: objs
    });
  }

  setObjectInsertInfo(object) {
    this.setState({
      insertObjectInfo: object
    });
  }

  clearObjectInsertInfo() {
    this.setState({
      insertObjectInfo: false
    });
  }

  getWorkArea() {
    const { data, insertObjectInfo, objects, selected } = this.state;

    const { width, height, X, Y } = calcAmbientArea(data);

    let active = this === Globals.ElementConnector._currentWorkArea;

    let isSelected =
      Number.parseInt(selected, 10) === Number.parseInt(data.id, 10);

    return (
      <div
        className={`workArea${active ? ' active' : ' inactive'}${
          isSelected ? ' selected' : ''
        }`}
        style={{ left: X, top: Y, width, height }}
        onClick={this.setActiveWorkArea.bind(this)}
      >
        <Grid className='content' width={data.width} height={data.height}>
          <div className='workAreaName'>
            <div className='workAreaNameContent'>{data.name}</div>
          </div>
          <WorkAreaObjectInsert data={insertObjectInfo} ambient={data.id} />
          <WorkAreaObjects objects={objects} />
        </Grid>
      </div>
    );
  }

  render() {
    return this.getWorkArea();
  }
}
