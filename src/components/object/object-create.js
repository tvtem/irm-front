import React from 'react';
import Globals from '../../modules/Globals';
import { Modal, Button } from 'react-bootstrap';
import { Api } from '../../services/index';

export default class ObjectCreator extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      visible: false,
      elements: []
    };

    Globals.ElementConnector['_objectCreator'] = this;
  }

  show() {
    this.setState({ visible: true });
  }
  hide() {
    this.setState({ visible: false });
  }
  toggle() {
    this.setState({ visible: this.state.visible ? false : true });
  }

  async componentWillMount() {
    const { data } = await Api.get('objects/list');
    this.setState({ elements: data });
  }

  // elements = [
  //   {
  //     id: 'el-q-01',
  //     name: 'Vaso',
  //     size: '1x1',
  //     img: 'svg/room-vessel.svg',
  //     imgWidth: 90,
  //     imgHeight: 250,
  //     imgX: 58,
  //     imgY: -177,
  //     imgFlipX: 58,
  //     imgFlipY: -177,
  //     X: 5,
  //     Y: 3,
  //     flip: false
  //   },
  //   {
  //     id: 'el-q-03',
  //     name: 'Quarto',
  //     size: '13x13',
  //     img: 'svg/room.svg',
  //     imgWidth: 1500,
  //     imgHeight: 1500,
  //     imgX: -218,
  //     imgY: -635,
  //     imgFlipX: -218,
  //     imgFlipY: -635,
  //     X: 0,
  //     Y: 0,
  //     flip: false
  //   },
  //   {
  //     id: 'penteadeira-01',
  //     name: 'Penteadeira',
  //     size: '4x2',
  //     img: 'svg/DEMO_PENTEADEIRA.svg',
  //     imgWidth: 460,
  //     imgHeight: 450,
  //     imgX: -43,
  //     imgY: -290,
  //     imgFlipX: -65,
  //     imgFlipY: -215,
  //     X: 0,
  //     Y: 0,
  //     flip: false
  //   },
  //   {
  //     id: 'cama-01',
  //     name: 'Cama 01',
  //     size: '4x3',
  //     img: 'svg/DEMO_CAMA.svg',
  //     imgWidth: 460,
  //     imgHeight: 450,
  //     imgX: -105,
  //     imgY: -179,
  //     imgFlipX: -126,
  //     imgFlipY: -150,
  //     X: 0,
  //     Y: 0,'
  //     flip: false
  //   },
  //   {
  //     id: 'guarda-roupa-01',
  //     name: 'Guarda Roupa',
  //     size: '1x4',
  //     img: 'svg/DEMO_GUARDAROUPA.svg',
  //     imgWidth: 480,
  //     imgHeight: 480,
  //     imgX: -115,
  //     imgY: -240,
  //     imgFlipX: -40,
  //     imgFlipY: -310,
  //     X: 0,
  //     Y: 0,
  //     flip: false
  //   }
  // ];

  addElement(el) {
    let newid = `${el.id}-${Date.now()}`;
    let element = Object.assign({}, el);
    element.id = newid;

    Globals.ElementConnector._currentWorkArea.setObjectInsertInfo(element);
    this.hide();
  }

  getElements() {
    const { elements } = this.state;
    let list = Object.values(elements).map((el, i) => {
      return (
        <div className='col-xs-12 col-md-6' key={i}>
          <div>{el.name}</div>
          <div>{el.size}</div>
          <div>{el.img}</div>
          <div className='form-group'>
            <Button
              bsStyle='primary'
              onClick={() => {
                this.addElement(el);
              }}
            >
              Adicionar
            </Button>
          </div>
        </div>
      );
    });

    return <div className='row'>{list}</div>;
  }

  getModal() {
    return (
      <Modal show={this.state.visible} onHide={this.hide.bind(this)}>
        <Modal.Header closeButton>
          <Modal.Title>Adicionar Elemento</Modal.Title>
        </Modal.Header>
        <Modal.Body>{this.getElements()}</Modal.Body>
        <Modal.Footer>
          <Button variant='secondary' onClick={this.hide.bind(this)}>
            Fechar
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }

  render() {
    return this.getModal();
  }
}
