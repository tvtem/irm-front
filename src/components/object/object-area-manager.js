import React from 'react';
import Globals from '../../modules/Globals';
import { FormGroup, FormControl, Button, ControlLabel } from 'react-bootstrap';
import Core from '../../modules/Core';
import { Api } from '../../services/index';

export default class WorkAreaManager extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      id: props.id ? props.id : 0,
      selected: 0,
      areas: false
    };

    Globals.ElementConnector._areaManager = this;
  }

  componentDidMount() {
    this.loadAmbients();
  }

  componentWillReceiveProps(props) {
    this.setState({
      id: props.id ? props.id : 0,
      selected: 0
    });
  }

  async loadAmbients() {
    let req = await Api.get('ambients', {
      project: this.state.id
    });

    if (req && req.data) {
      console.log(req.data);
      this.setState({
        areas: req.data
      });
    }
  }

  changeSelected(selected = 0) {
    this.setState({ selected });
  }

  async updateAreaInfo(info, value) {
    const { selected, areas } = this.state;

    for (let i = 0; i < areas.length; i++) {
      const _area = areas[i];
      if (Number.parseInt(_area.id, 10) === Number.parseInt(selected, 10)) {
        _area[info] = value;
      }

      if (Globals.Timers.updateArea) clearTimeout(Globals.Timers.updateArea);

      let timer = setTimeout(async () => {
        await Api.post('ambients/update', _area);
      }, 2000);

      Globals.Timers.updateArea = timer;
    }

    this.setState({ areas });
  }

  getEditor() {
    const { selected, areas } = this.state;
    let area = null;

    for (let i = 0; i < areas.length; i++) {
      const _area = areas[i];
      if (Number.parseInt(_area.id, 10) === Number.parseInt(selected, 10))
        area = _area;
    }

    return (
      <div className='info'>
        <FormGroup>
          <ControlLabel>ID da Área: {area.id}</ControlLabel>
        </FormGroup>

        <FormGroup>
          <ControlLabel>Nome da Área</ControlLabel>
          <FormControl
            type='text'
            defaultValue={area.name}
            placeholder='Nome da Área'
            onChange={el => {
              this.updateAreaInfo('name', el.currentTarget.value);
            }}
          />
        </FormGroup>

        <div className='row'>
          <div className='col-xs-6'>
            <FormGroup>
              <ControlLabel>Largura da Área (Em blocos)</ControlLabel>
              <FormControl
                type='number'
                defaultValue={area.width}
                placeholder='Largura da Área (Em blocos)'
                onChange={el => {
                  this.updateAreaInfo('width', el.currentTarget.value);
                }}
              />
            </FormGroup>
          </div>
          <div className='col-xs-6'>
            <FormGroup>
              <ControlLabel>Altura da Área (Em blocos)</ControlLabel>
              <FormControl
                type='number'
                defaultValue={area.height}
                placeholder='Altura da Área (Em blocos)'
                onChange={el => {
                  this.updateAreaInfo('height', el.currentTarget.value);
                }}
              />
            </FormGroup>
          </div>
          <div className='col-xs-6'>
            <FormGroup>
              <ControlLabel>Pos X da Área (Em blocos)</ControlLabel>
              <FormControl
                type='number'
                defaultValue={area.x}
                placeholder='Pos X da Área (Em blocos)'
                onChange={el => {
                  this.updateAreaInfo('x', el.currentTarget.value);
                }}
              />
            </FormGroup>
          </div>
          <div className='col-xs-6'>
            <FormGroup>
              <ControlLabel>Pos Y da Área (Em blocos)</ControlLabel>
              <FormControl
                type='number'
                defaultValue={area.y}
                placeholder='Pos Y da Área (Em blocos)'
                onChange={el => {
                  this.updateAreaInfo('y', el.currentTarget.value);
                }}
              />
            </FormGroup>
          </div>
        </div>

        <div className='actions'>
          <Button bsStyle='primary' onClick={() => this.changeSelected()}>
            Voltar
          </Button>
        </div>
      </div>
    );
  }

  createArea() {
    let newArea = {
      id: 5,
      name: 'Nova Área',
      width: 2,
      height: 2,
      X: 0,
      Y: 0
    };

    let selected = newArea.id;
    let areas = this.state.areas;
    areas.push(newArea);

    this.setState({ areas, selected });
  }

  getSelector() {
    const { areas } = this.state;

    return areas ? (
      <div className='info'>
        <FormGroup>
          <label>Ambientes</label>
          <FormControl
            componentClass='select'
            placeholder='Selecione o Ambiente'
            onChange={e => {
              this.changeSelected(e.currentTarget.value);
            }}
          >
            <option value='0'>Selecione...</option>
            {areas.map((area, i) => {
              return (
                <option key={i} value={area.id}>
                  {area.name}
                </option>
              );
            })}
          </FormControl>
        </FormGroup>

        <div className='actions'>
          <Button bsStyle='primary' onClick={this.createArea.bind(this)}>
            Criar Área
          </Button>
        </div>
      </div>
    ) : (
      Globals.loader()
    );
  }

  getController() {
    const { selected } = this.state;

    return (
      <div className='areaManager'>
        <div className='title'>Ger. Ambientes</div>

        <div className='areaData'>
          {selected ? this.getEditor() : this.getSelector()}
        </div>
      </div>
    );
  }

  render() {
    const { areas, selected } = this.state;
    return (
      <>
        <div id='appRoot' className='appRoot'>
          <title>Interative Room Manager</title>
          <Core areas={areas} selected={selected} />
        </div>
        {this.getController()}
      </>
    );
  }
}
