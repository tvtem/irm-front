import React from 'react';
import Globals from '../../modules/Globals';
import { Button, ButtonToolbar } from 'react-bootstrap';
import { Api } from '../../services';

export default class WorkAreaObjectInsert extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      objectInfo: props.data ? props.data : false,
      ambient_id: props.ambient,
      moving: false,
      flip: false
    };

    let _controller = this;

    window.addEventListener('mousemove', event => {
      _controller.elementMouseMove(event);
    });

    window.addEventListener('mouseup', event => {
      _controller.elementMouseUp();
    });
  }

  componentWillReceiveProps(props) {
    this.setState({
      objectInfo: props.data ? props.data : false
    });
  }

  elementMouseDown(event, id) {
    event.preventDefault();

    let rect = document.getElementById(id).getBoundingClientRect();
    let rpy = event.clientY - rect.top;
    let rpx = event.clientX - rect.left;

    this.setState({ moving: true, modPos: { top: rpy, left: rpx } });
  }

  elementMouseUp(event) {
    this.setState({ moving: false });
  }

  elementMouseMove(event) {
    if (this.state.moving) {
      let elements = Array.from(document.querySelectorAll('.grid-mark'));

      let linkCoords = elements.map(link => {
        link.classList.remove('hl');
        let rect = link.getBoundingClientRect();
        return [rect.x, rect.y];
      });

      let distances = [];

      linkCoords.forEach(linkCoord => {
        let x = Number.parseInt(event.clientX, 10) - this.state.modPos.left;
        let y = Number.parseInt(event.clientY, 10) - this.state.modPos.top;
        let distance = Math.hypot(linkCoord[0] - x, linkCoord[1] - y);
        distances.push(Number.parseInt(distance, 10));
      });

      let closestLinkIndex = distances.indexOf(Math.min(...distances));

      let nearestElement = elements[closestLinkIndex];
      nearestElement.classList.add('hl');

      let x = nearestElement.offsetLeft / Globals.Settings.GridSize;
      let y = nearestElement.offsetTop / Globals.Settings.GridSize;

      let objInfo = this.state.objectInfo;
      objInfo.X = x;
      objInfo.Y = y;

      this.setState({
        objectInfo: objInfo
      });
    }
  }

  cancelElementInsert() {
    Globals.ElementConnector._currentWorkArea.setObjectInsertInfo(false);
  }

  async insertElement(element) {
    const { ambient_id } = this.state;
    const ambient_object = Object.assign(element, { ambient_id });
    debugger;
    await Api.post('ambients/object/save', ambient_object);
    Globals.ElementConnector._currentWorkArea.addObject(element);
    this.cancelElementInsert();
  }

  async toggleElementDirection() {
    const { objectInfo } = this.state;
    const flip = objectInfo.flip;

    let nFlip = flip ? false : true;
    objectInfo.flip = nFlip;

    await this.setState({
      objectInfo
    });
  }

  formElement() {
    const { objectInfo } = this.state;
    if (!objectInfo) return null;

    // let flip = objectInfo.flip;
    let flip = objectInfo.flip ? true : false;

    let size = objectInfo.size.split('x');
    let w = Number.parseInt(size[flip ? 1 : 0], 10) * Globals.Settings.GridSize;
    let h = Number.parseInt(size[flip ? 0 : 1], 10) * Globals.Settings.GridSize;

    let x = objectInfo.X * Globals.Settings.GridSize;
    let y = objectInfo.Y * Globals.Settings.GridSize;

    return (
      <div
        id={objectInfo.id}
        className={`element-insert${flip ? ' flip' : ''}`}
        style={{ width: w, height: h, left: x, top: y }}
        onMouseDown={event => {
          this.elementMouseDown(event, objectInfo.id);
        }}
        onMouseUp={event => {
          this.elementMouseUp(event);
        }}
      >
        <div
          className='element-insert-tile'
          style={{
            backgroundImage: `url(${objectInfo.img})`,
            left: flip ? objectInfo.imgFlipX : objectInfo.imgX,
            top: flip ? objectInfo.imgFlipY : objectInfo.imgY,
            width: objectInfo.imgWidth,
            height: objectInfo.imgHeight
          }}
        >
          <div className='element-insert-details'>
            <div className='element-insert-actions'>
              <ButtonToolbar>
                <Button
                  bsStyle='success'
                  bsSize='large'
                  onClick={() => this.insertElement(objectInfo)}
                >
                  <i className='fas fa-check' />
                </Button>
                <Button
                  bsStyle='primary'
                  bsSize='large'
                  onClick={this.toggleElementDirection.bind(this)}
                >
                  <i className='fas fa-sync-alt' />
                </Button>
                <Button
                  bsStyle='danger'
                  bsSize='large'
                  onClick={this.cancelElementInsert.bind(this)}
                >
                  <i className='fas fa-times' />
                </Button>
              </ButtonToolbar>
            </div>
            PosX: {objectInfo.X}, PosY: {objectInfo.Y}
          </div>
        </div>

        <div className='element-controls'>
          <i className='fas fa-arrow-alt-down down' />
          <i className='fas fa-arrow-alt-up up' />
          <i className='fas fa-arrow-alt-left left' />
          <i className='fas fa-arrow-alt-right right' />
        </div>
      </div>
    );
  }

  render() {
    return <>{this.formElement()}</>;
  }
}
