import React, { useState, useEffect, useRef } from 'react';
import { Modal, Button } from 'react-bootstrap';
import { Api } from '../../services/index';

const TITULO_PADRAO = 'Digite um titulo';
const MENSAGEM_PADRAO = 'Digite uma mensagem';

const ObjectMessage = props => {
  const pTitulo = useRef();
  const pMensagem = useRef();
  const [titulo, setTitulo] = useState('');
  const [mensagem, setMensagem] = useState('');

  const [visible, setVisible] = useState(false);

  useEffect(() => {
    if (props.idObject) {
      findMessages(props.idObject).then(resposta => {
        const { titulo, mensagem } = resposta.data[0];

        setTitulo(titulo ? titulo : TITULO_PADRAO);
        setMensagem(mensagem ? mensagem : MENSAGEM_PADRAO);
        setVisible(true);
      });
    }
  }, [props.show]);

  const findMessages = async idObject => {
    const resposta = await Api.post('message/find', {
      idObject
    });
    return await resposta;
  };

  const saveMessages = async () => {
    const res = await Api.post('message/update', {
      titulo: pTitulo.current.textContent,
      mensagem: pMensagem.current.textContent,
      idObject: props.idObject
    });

    if (res.status !== 200) {
      alert('Houve um problema na hora de salvar.');
    }

    setVisible(false);
  };

  return (
    <Modal show={visible}>
      <Modal.Header closeButton onHide={() => setVisible(false)}>
        <Modal.Title>
          <p contentEditable ref={pTitulo}>
            {titulo}
          </p>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <p contentEditable ref={pMensagem}>
          {mensagem}
        </p>
      </Modal.Body>
      <Modal.Footer>
        <Button bsStyle='success' onClick={saveMessages}>
          Salvar
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default ObjectMessage;
