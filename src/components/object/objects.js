import React from 'react';
import ObjectMessage from './object-message';
import { calcObjectArea } from '../utils/calculos';

export default class WorkAreaObjects extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      idObject: '',
      show: false,
      objects: props.objects ? props.objects : false
    };
  }

  componentWillReceiveProps(props) {
    this.setState({
      objects: props.objects ? props.objects : false
    });
  }

  formElement(element, index) {
    const { width, height, x, y } = calcObjectArea(element);

    return (
      <div
        key={index}
        id={element.id_object}
        className={`element${element.flip ? ' flip' : ''}`}
        style={{
          width: width,
          height: height,
          left: x,
          top: y,
          zIndex: element.zIndex
        }}
        onClick={element.zIndex !== 1 ? this.modalMessage : () => {}}
      >
        <div
          className={`element-tile ${
            element.zIndex !== 1 ? 'element-focus' : ''
          }`}
          style={{
            backgroundImage: `url(${element.img})`,
            left: element.flip ? element.imgFlipX : element.imgX,
            top: element.flip ? element.imgFlipY : element.imgY,
            width: element.imgWidth,
            height: element.imgHeight
          }}
        />
      </div>
    );
  }

  modalMessage = event => {
    const idObject = event.currentTarget.id;
    this.setState({ idObject: idObject, show: !this.state.show });
  };

  getObjects() {
    let objs = this.state.objects;
    let elements = objs.map((el, i) => {
      return this.formElement(el, i);
    });

    return elements;
  }

  render() {
    return (
      <>
        <ObjectMessage idObject={this.state.idObject} show={this.state.show} />
        {this.getObjects()}
      </>
    );
  }
}
