import React from 'react'
import moment from 'moment'
import * as Services from './services'


export const CURR_YEAR = moment.unix(Date.now() / 1000).format('YYYY')

// API End-point
// Localhost For DEV only
export const API_URL = 'http://localhost:3466';
//export const API_URL = 'https://api.nulbox.com';

// IMG Service
// Localhost For DEV only
export const IMG_URL = 'http://localhost:3466/img/';
//export const IMG_URL = 'https://img.nulbox.com/';

// FILE Service
// Localhost For DEV only
export const FILE_URL = 'http://localhost:3466/file/';
//export const FILE_URL = 'https://api.nulbox.com/file/';//

// Gets current session info for user
export const getUserData = () => {
    let data = Services.Storage.get('USER_SESSION')
    USER_DATA = JSON.parse(data)
}

export let USER_DATA = {}

export const USER_ROLE = {
    Admin: 4,
    Master: 3,
    User: 1,
    Guest: 0
}

// DEV MODE?
// This affects elements on screen that may show only for developers. Turning it into false, will remove any element for dev only.
// WARNING! DO NOT use this enabled in production.
const DEV_MODE = true;

export class DevModeOnly extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            children: props.children,
        }
    }

    render() {
        if (DEV_MODE) {
            return (<span className="devMode">{this.props.children}</span>)
        }
        else
            return null
    }
}

// GLOBAL FUNCTIONS
//Prototypes.Override()
