import { API_URL, USER_DATA } from '../settings';

import React from 'react';
import axios from 'axios';

class ApiService extends React.Component {
    async get(url, data) {
        try {
            let udata = USER_DATA
            const response = await axios.get(`${API_URL}/${url}`, {
                params: data,
                headers: {
                    'x-access-token': udata && udata.token ? udata.token : false
                }
            })
            return response;
        } catch (error) {
            console.error(error);
        }
    }

    async post(url, data) {
        try {
            let udata = USER_DATA
            const params = new URLSearchParams();
            Object.keys(data).forEach(function (key) {
                params.append(key, data[key])
            })
            const response = await axios.post(`${API_URL}/${url}`, params, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'x-access-token': udata && udata.token ? udata.token : false
                }
            })
            return response;
        } catch (error) {
            return error;
        }
    }

    async postFile(url, data) {
        try {
            let udata = USER_DATA
            const response = await axios.post(`${API_URL}/${url}`, data, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'x-access-token': udata && udata.token ? udata.token : false
                }
            })
            return response;
        } catch (error) {
            return error;
        }
    }
}

export default ApiService