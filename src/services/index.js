import AuthService from './auth'
import StorageService from './storage'
import ApiService from './api'

const Storage = new StorageService()
const Auth = new AuthService()
const Api = new ApiService()

export { Storage, Auth, Api }