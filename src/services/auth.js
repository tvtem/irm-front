import * as Settings from '../settings';

import Noty from 'noty';
import React from 'react';
import * as Services from '../services/index';
import '../sass/noty.scss';

class AuthService extends React.Component {
  componentDidMount() {}

  sendInvalidLogin() {
    new Noty({
      title: 'Login inválido!',
      text: 'Usuário ou Senha incorretos.',
      layout: 'topCenter',
      timeout: '3000',
      type: 'error'
    }).show();
  }

  sendInvalidSignUp() {
    new Noty({
      title: 'Settings.APP_LANG.get(1292)',
      text: 'Settings.APP_LANG.get(1293)',
      layout: 'topCenter',
      timeout: 'Settings.APP_LANG.get(1295)',
      type: 'error'
    }).show();
  }

  sendSignUpEmailInUse() {
    new Noty({
      title: 'Settings.APP_LANG.get(1292)',
      text: 'Settings.APP_LANG.get(1297)',
      layout: 'topCenter',
      timeout: 'Settings.APP_LANG.get(1295)',
      type: 'error'
    }).show();
  }

  sendUnknownAlert() {
    new Noty({
      title: 'Erro de Login',
      text: 'Não foi possível fazer Login.',
      layout: 'topCenter',
      timeout: '3000',
      type: 'error'
    }).show();
  }

  async doLogin(data) {
    let req = await Services.Api.post('login', data);

    if (req.response && req.response.status === 401) {
      // Login invalid
      this.sendInvalidLogin();
      return false;
    }

    let rdata = req.data;

    if (!rdata || rdata.error) {
      if (!rdata) this.sendUnknownAlert();
      else if (rdata.error === 'INVALID_LOGIN') this.sendInvalidLogin();
      return false;
    }

    return this.createSession(rdata);
  }

  async doSignUp(data) {
    let req = await Services.Api.post('signup', data);

    if (req.data) {
      if (req.data.success) {
        return this.doLogin(data);
      } else if (req.data.status === 2) {
        this.sendSignUpEmailInUse();
        return false;
      }
    }

    this.sendInvalidSignUp();
    return false;
  }

  createSession(data) {
    let session = data;
    // add settings to session
    Services.Storage.set('USER_SESSION', JSON.stringify(session));
    let sdata = Services.Storage.get('USER_SESSION');
    console.log(sdata);
    return true;
  }

  async doLogout(data) {
    let req = await Services.Api.post('logout', data);

    if (req.response && req.response.status === 401) {
      // Login inválido

      return false;
    }

    // Login válido
    // Expira o token
    //console.log(req.data)

    return this.destroySession();
  }

  destroySession() {
    Services.Storage.del('USER_SESSION');
    return true;
  }

  async isLogged() {
    return await this.validLogin();
  }

  async validLogin() {
    let userData = Settings.USER_DATA;
    if (userData && userData.token && userData.token !== '') return true;
    return false;
  }

  async unlockPage() {
    return await this.isLogged();
  }
}

export default AuthService;
