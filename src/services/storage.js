export default class StorageService {
    set(key, value) {
        localStorage.setItem(key, value);
    }

    get(key) {
        return localStorage.getItem(key);
    }

    del(key) {
        return localStorage.removeItem(key);
    }
}