import React from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import App from './modules/App';
import Preview from '../src/components/pages/preview/preview';
import ErrorPage404 from '../src/components/pages/404/404';

const Routes = () => {
  return (
    <BrowserRouter basename='.'>
      <Switch>
        <Route exact path='/' component={App}></Route>
        <Route path='/preview/:id' component={Preview}></Route>
        <Route path='/404' component={ErrorPage404}></Route>
        <Redirect path='*' to='/404'></Redirect>
      </Switch>
    </BrowserRouter>
  );
};

export default Routes;
